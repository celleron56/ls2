from gui import *
import copy
class galaxy():
    def __init__(self):
        self.planets = []
    def addplanet(self,planet):
        self.planets.append(planet)
    def draw(self,timer=0,viewport="none"):
         for i in self.planets:
             i.draw(timer,viewport)
    
class planetobj():
    def __init__(self,x=1,y=0,seed=9234,age=68,color=(0,1,0),name="planet",description=" 127.0.0.1 , home sweet home  never will thy become infected with the virus that has a closedcure", attach="no",size=10):
     self.seed = seed
     self.age = age
     self.gmult = 0.1
     self.color = color
     self.colormap = []
     self.name = name
     self.description = description
     self.gplanet()
     self.x = x
     self.y = y
     self.xposition = self.x
     self.yposition = self.y
     self.attach = attach
     self.size=size
     self.atmospheredist = size * 0.5
     self.gravityatbase = self.size * 0.1
     self.time = 0
    def gposition(self,time):
        self.time = time
        if not self.attach == "no":
            self.attach.gposition(time)
            self.xposition = (math.sin(((time * self.x) % 361 )* 0.01745329) * self.y) + self.attach.xposition
            self.yposition = (math.cos(((time * self.x) % 361) * 0.01745329) * self.y) + self.attach.yposition
        else:
             self.xposition = self.x
             self.yposition = self.y
     
    def draw(self,time,viewport="none"):
        if not self.attach == "no":
            self.attach.gposition(time)
            self.xposition = (math.sin(((time * self.x) % 361 )* 0.01745329) * self.y) + self.attach.xposition
            self.yposition = (math.cos(((time * self.x) % 361) * 0.01745329) * self.y) + self.attach.yposition
        else:
                 self.xposition = self.x
                 self.yposition = self.y
        self.mkplanet(self.xposition,self.yposition,self.size,viewport)
        self.batch.draw()
    def gplanet(self):
     self.batch = pyglet.graphics.Batch()
     opensimplex.seed(self.seed)
     done = 0
     xc = 0
     c = 0
     self.terrain = []
     t = opensimplex.noise2(x=0, y=self.age + 21)
     self.colormap.append(opensimplex.noise2(x=0, y=self.age + 21 + t))
     for i in range(1,19):
         t = opensimplex.noise2(x=t, y=self.age + 21)
         self.colormap.append(t)
     start = opensimplex.noise2(x=0, y=self.age)
     while (done == 0 or xc < 50) and not xc > 100 :
         xc = xc + 1
         value = opensimplex.noise2(x=xc, y=self.age)
         self.terrain.append(value)
         if xc > 36:
          if math.floor(value * 100 ) == math.floor(start * 100):
             self.done = 1
    
    
    def mkplanet(self, x=1,y=0,size=10 , viewport="none"):
        self.trias = []
        LOD = 1
        if not viewport == "none":
            x = (x * (viewport.getzm() * 0.01))+ viewport.getcx()
            y = (y * (viewport.getzm() * 0.01)) + viewport.getcy()
            LOD = (200 / viewport.getzm())
            if LOD < 1 or viewport.getzm() > 20 :
                LOD = 1
            if LOD > 1.5 and viewport.getzm() > 1:
                LOD = 1.5
            size = size * ( viewport.getzm() * 0.1)
        disttocam = math.dist((x,y),(0,0)) 
        if disttocam < 3000 + self.size * 2 : 
            global debug
            corner1 = (x,y)
            self.trias = []
            t = 1
            deltatheta = 360 / math.ceil(len(self.terrain) / LOD)        
            for counter in range(0,(math.ceil(len(self.terrain) / LOD))):
                i = self.terrain[counter % len(self.terrain)]
                precolor = (math.ceil((self.color[0] * self.colormap[t])*100) , math.floor((self.color[1] * self.colormap[t])* 100) ,math.floor(( self.color[2] * self.colormap[t]) * 100 ))
                theta1 = ((deltatheta * counter)/180) * 3.1459
                theta2 = (deltatheta * (counter + 1)/180) * 3.1459
                radius = self.terrain[counter % len(self.terrain)] * (size * 0.1) + size
                radius2 = self.terrain[(counter + 1 )% len(self.terrain)] * (size * 0.1) + size
                corner2 = (x + radius * math.cos(theta1), y + radius * math.sin(theta1))
                corner3 = ( (x + radius2 * math.cos(theta2)), (y + radius2 * math.sin(theta2)))
                self.trias.append(shapes.Triangle(x,y,corner2[0], corner2[1], corner3[0], corner3[1], color=precolor, batch=self.batch)     )#color=(255, counter % 255, 255)
                t = (t + 1)  % 19
                
                if debug == 1:
                 self.trias.append(shapes.Circle(  corner2[0], corner2[1], 2, color=(255 , counter % 255, 40), batch=self.batch)     )
                 self.trias.append(shapes.Circle(  corner3[0],  corner3[1], 2, color=(255,255, 255), batch=self.batch)     )

                

milkyway = galaxy()