import time
import opensimplex
import math
import pyglet
import glooey
import sys 
import random
random.seed(10)
from pyglet import shapes
from pyglet import clock
from numba import njit
from numba import jit
from numba.experimental import jitclass
import vector
import socket
import getopt
debug = False

parameters = []
try:
  parameters, args = getopt.getopt(sys.argv[1:],"h")
except getopt.GetoptError:
  pass
  print("please contribute to the libre space 2d project at gitlab.com/celleron56/ls2")

for opt, arg in parameters:
  if opt == '-h':
     print (' controls = drag to move camera , a_button to go to VAB , pgeup / pgedown select part  shift+ plus place part , minus delete part , arrows to move cursor in VAB and to control rocket in playing mode , button with eye on it to switch views , zoom glass buttons to zoom in / out ')
     sys.exit()
  else:
      print("usage (path)main.py see  -h for information")






def rotvec(x,y, degrees, origin=(0, 0)):
    radians = (degrees % 361) * 0.01745329
    offset_x, offset_y = origin
    adjusted_x = (x - offset_x)
    adjusted_y = (y - offset_y)
    cos_rad = math.cos(radians)
    sin_rad = math.sin(radians)
    qx = offset_x + cos_rad * adjusted_x + sin_rad * adjusted_y
    qy = offset_y + -sin_rad * adjusted_x + cos_rad * adjusted_y

    return qx, qy

class gamedata():
    def __init__(self):
        self.mousepressed = 0
        self.menu = False
        self.play = True
        self.editor = False
game = gamedata()

pi = 3.1459
debug = 0
scr_X = 800
scr_Y = 600
window =  pyglet.window.Window(scr_X,scr_Y)
def update(dt):
    global window 
    window.dispatch_events()
pyglet.clock.schedule_interval(update, 1/30.0)   
gui_batch = pyglet.graphics.Batch()
gui = glooey.Gui(window,batch=gui_batch)
zoomthelevel = 1
def zoomlc(i):
    global zoomthelevel
    zoomlevel = zoomthelevel + i

class starfield():
    def __init__(self):
        self.starfield = pyglet.graphics.Batch()
        self.starlist = []
        self.image = pyglet.image.load('assets/images/star.png')
        for i in range(1,100):
            sprite = pyglet.sprite.Sprite(self.image, 0, 0, batch=self.starfield)
            sprite.update(scale=0.1)
            self.starlist.append([sprite,random.randint(-800,800),random.randint(-800,800)])
    def draw(self,viewport):
        for i in self.starlist :
            i[0].update(x= (i[1] +  ((2* ((viewport.getcx() * 0.01)) % 400)) - 400) , y=i[2] + ((2* ((viewport.getcy() * 0.01)) % 400)) - 400)
        self.starfield.draw()

starfieldbg = starfield()
try:
    if  socket.gethostname() == "H4xx0r134":
        developingdonotdisturb =  True
    else:
        developingdonotdisturb =  False
except:
    developingdonotdisturb = False
    
    

ambient= pyglet.media.load('assets/audio/spacemusic.mp3', streaming=False)
ambientplayer = pyglet.media.Player()
ambientplayer.loop= True 
ambientplayer.queue(ambient)
if developingdonotdisturb == False:
    ambientplayer.play()