import math
gravityconstant = 0.3
class gravityobject():
    def __init__(self,position=(0,0),speed=(0,0),mass=10):
        self.x = position[0]
        self.y = position[1]
        self.xvel = speed[0]
        self.yvel = speed[1]
        self.mass = mass

def findgravityacc(gravityob1,gravityob2):
    global gravityconstant
    precalc1 =  (gravityob2.x - gravityob1.x)
    precalc1 = precalc1 * precalc1
    precalc2 = (gravityob2.y - gravityob1.y)
    precalc2 = precalc2 * precalc2
    distance =  math.sqrt( (precalc1)+ (precalc2))
    
    return (( gravityob2.mass* gravityconstant) / (distance * distance))

def findgravityangle(gravityob1,gravityob2):
    gravityob2xes = ( gravityob2.x- ( gravityob1.x+ gravityob2.x) )
    return ( math.atan(( gravityob2.y -(gravityob1.y + gravityob2.y) ) / gravityob2xes ) + ((gravityob2xes < 0) * 180) )
def move(gravityob1,gravityob2 , gravityangle , gravity_acc,timestep):
    gravityob1.xvel += (( gravity_acc*math.cos(gravityangle)) * timestep)
    gravityob1.yvel += (( gravity_acc*math.sin(gravityangle)) * timestep)
    return gravityob1
def gravity(gravityob1,gravityob2):
    gravity_angle = findgravityangle(gravityob1,gravityob2)
    distance = 0
    gravity_acc = findgravityacc(gravityob1,gravityob2)
    timestep = 1
    newplanetobj =  move(gravityob1,gravityob2,gravity_angle,gravity_acc,timestep)
    return newplanetobj