from celestilib import *
path = 'a_button'
############costum buttons #################
class costumbutton(glooey.Button):
         global path
         Background = glooey.Image
         custom_base_image = pyglet.resource.image('assets/gui/'+ path +'base.png')
         custom_over_image = pyglet.resource.image('assets/gui/'+ path +'over.png')
         custom_down_image = pyglet.resource.image('assets/gui/'+ path  +'down.png')


class zoomoutbutton(glooey.Button):
     Background = glooey.Image

     custom_base_image = pyglet.resource.image('assets/gui/zoomoutbase.png')
     custom_over_image = pyglet.resource.image('assets/gui/zoomoutover.png')
     custom_down_image = pyglet.resource.image('assets/gui/zoomoutdown.png')

class switchviewbutton(glooey.Button):
     Background = glooey.Image

     custom_base_image = pyglet.resource.image('assets/gui/switchviewbase.png')
     custom_over_image = pyglet.resource.image('assets/gui/switchviewover.png')
     custom_down_image = pyglet.resource.image('assets/gui/switchviewdown.png')



class zoominbutton(glooey.Button):
     Background = glooey.Image

     custom_base_image = pyglet.resource.image('assets/gui/zoominbase.png')
     custom_over_image = pyglet.resource.image('assets/gui/zoominover.png')
     custom_down_image = pyglet.resource.image('assets/gui/zoomindown.png')


########################################################
grid = glooey.Grid(5, 5)
gui.add(grid)
path = 'a_button'
abtn = costumbutton()
zoutbtn = zoomoutbutton("")
zinbtn  = zoominbutton("")
swvbtn  = switchviewbutton("")
grid.add(1,4,zoutbtn)
grid.add(2,4,zinbtn)
grid.add(3,4,swvbtn)
grid.add(4,4,abtn)

