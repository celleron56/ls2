#!/usr/bin/python3
from render import camera
from spacecraft import *
import copy
from time import time
import pyglet.window.key as key
##### editor setup code#####
editor = editorstage()
cursorx = 0
cursory = 0
cursorselect = 0

editorpartlist = []
@abtn.event()
def on_click(widget):
    global game , editorpartlist , rocket ,active
    if game.play == True:
        game.play = False
        game.editor = True
        active = False
        editorpartlist = rocket.parts
    else:
        game.play = True
        game.editor = False
        #rocket.positionx = earth.xposition
       # rocket.positiony  = (earth.yposition + earth.size + 20)
        rocket.parts = editorpartlist
        
############ basic game logic  & rendering ###########
sun = planetobj(seed=322,color=(1.2,1.2,0),size=100,name="sun")
earth = planetobj(seed=91222,attach=sun,y=5050,size=20,name="moon")
moon = planetobj(x=12,seed=321,color=(0.5,0.5,0.5),y=1050,attach=earth,size=10,name="earth")
milkyway.addplanet(sun)
milkyway.addplanet(earth)
milkyway.addplanet(moon)
timer = 0
sun.gmult = 0.001
viewmode = 0
zoomlevel = 100
game.play = True
game.editor = False
game.menu = False
def rreset(timer):
    global rocket , earth
    rocket.positionx = earth.xposition
    rocket.positiony = earth.yposition 
#earth.gposition(2)
#rocket.positionx = earth.xposition
#rocket.positiony  = (earth.yposition + earth.size + 20)


viewport = camera(0,0,zoomlevel)
@zoutbtn.event
def on_click(widget):
     global zoomlevel
     global viewport
     if zoomlevel > 1:
        zoomlevel = zoomlevel * 0.5
        viewport.smoothchangezoomlevel(zoomlevel)



@window.event
def on_key_press(symbol, modifier):
    global rocket , timer , debug
    global cursorx , cursory , cursorselect , editorpartlist
    if game.editor == True:
        
        if symbol == key.UP:
            cursory += 1
            time.sleep(0.01)
        if symbol == key.DOWN:
            cursory += -1
            time.sleep(0.01)
        if symbol == key.LEFT:
            cursorx += -1
            time.sleep(0.01)
        if symbol == key.RIGHT:
            cursorx += 1
            time.sleep(0.01)
        if symbol == key.NUM_1:
            cursorselect = 0
        if symbol == key.NUM_2:
            cursorselect = 1
        if symbol == key.NUM_3:
            cursorselect = 2
        if symbol == key.PAGEDOWN:
            cursorselect = (cursorselect -1) % (len(partlist) - 1)
        if symbol == key.PAGEUP:
            cursorselect = (cursorselect + 1 ) % (len(partlist) - 1)
        if symbol == key.MINUS:
            for i in range(0,len(editorpartlist) - 1):
                if editorpartlist[i][1] == cursorx and editorpartlist[i][2] == cursory :
                    editorpartlist.pop(i)
            
        if symbol == key.PLUS:
            editorpartlist.append((copy.deepcopy(cursorselect),copy.deepcopy(cursorx),copy.deepcopy(cursory)))
    elif game.play == True :
        if symbol == key.UP:
            rocket.accelerate(10)
        if symbol == key.DOWN:
            rocket.accelerate(-10)
        if symbol == key.LEFT:
            rocket.rotate(10)
        if symbol == key.RIGHT:
            rocket.rotate(-10)
        if symbol == key.PAGEDOWN and debug == True:
            rreset(timer)
            print(rocket.velocity)
        if symbol == key.NUM_2:
            pass
        if symbol == key.NUM_3:
            pass
    
    ######end
@zinbtn.event
def on_click(widget):
     global zoomlevel
     global viewport
     if zoomlevel < 1600:
         zoomlevel = zoomlevel * 2
         viewport.smoothchangezoomlevel(zoomlevel)

@swvbtn.event
def on_click(widget):
     global viewmode
     global viewport
     viewmode = (viewmode + 1 ) % 5
     viewport.camerax = 0
     viewport.cameray = 0
     viewport.attachx = 0
     viewport.attachy = 0
     if viewmode == 0:
         viewport.camerax = 0
         viewport.cameray = 0
         viewport.attachx = 0
         viewport.attachy = 0
     


@window.event
def on_mouse_drag(x, y, dx, dy, buttons, modifiers):
        global viewport
        global zoomlevel
        newcamerax = viewport.camerax + (dx *  ((1.7  )-(zoomlevel * 0.001) ))
        newcameray = viewport.cameray + (dy * (1.7-(zoomlevel*0.001) ))
        viewport.camerax = newcamerax
        viewport.cameray = newcameray
        if game.editor == True:
            if viewport.camerax > 1000 :
                viewport.camerax = 1000
            if viewport.cameray > 1000:
                viewport.cameray = 1000
            if viewport.camerax < 0:
                viewport.camerax = 0
            if viewport.cameray < 0:
                viewport.cameray = 0
active = False





@window.event
def on_draw():
    
    global timer , active , editorpartview , cursorselect
    global viewport
    global zoomthelevel
    global zoomlevel
    global editorpartlist
    global rocket
    if game.play == True:
        if math.floor(time() * 100) == math.floor(time() * 20) * 5:
            timer = (timer + 0.01) % 361
        #print(timer)

        ### zoom handler
        viewport.updzoom()
        #####endif
        window.clear()
        starfieldbg.draw(viewport)
        milkyway.draw(timer,viewport)
        if active == False:
            rreset(timer)
            if timer > 10:
                active = True
        rocket.draw(viewport=viewport)
        rocket.run(milkyway.planets)
        
        gui_batch.draw()
        if viewmode == 1:
            viewport.attach(50,50)
            viewport.camerax =  0 -rocket.positionx
            viewport.cameray = 0 - rocket.positiony
           # viewport.zoomlevel = 100
           # viewport.smoothzoomtarget = 100
            #viewport.smoothzoodistance = -10
        if viewmode == 2:
            viewport.attach(milkyway.planets[1].xposition,milkyway.planets[1].yposition)
            viewport.zoomlevel = 100
            viewport.smoothzoomtarget = 100
            viewport.smoothzoodistance = 0
        if viewmode == 3:
            viewport.attach(milkyway.planets[2].xposition,milkyway.planets[2].yposition)     
            viewport.camerax = 0
            viewport.cameray = 0
            viewport.zoomlevel = 100
            viewport.smoothzoomtarget = 100
            viewport.smoothzoodistance = 0
    elif game.editor == True:
        timer = (timer + 0.1) % 361
        #print(timer)
        time.sleep(0.01)
        viewport.updzoom()
        window.clear()
        editor.draw(viewport)
        partlistprime = []
        partlistprime = copy.deepcopy(editorpartlist)
        partlistprime.append(("cursor",cursorx,cursory))
        editorrocket = rocketship(0,0,parts=partlistprime)
        editorrocket.draw(viewport)
        if (timer % 5 == 1):
            rocket = copy.deepcopy(editorrocket)
        del(partlistprime)
        viewport.zoomlevel = 1000
        viewport.smoothzoomtarget = 1000
        viewport.smoothzoodistance = 0
        editorpartview.draw(cursorselect)
        gui_batch.draw()
        
    elif game.menu == True:
        pass
    else:
        pass

   # print(rocket.positionx,rocket.positiony)
pyglet.app.run()





