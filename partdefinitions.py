tank = partblueprint('assets/images/verticaltank.png',10,fuellevel=100)
nosecone = partblueprint('assets/images/nosecone.png',10,fuellevel=25)
solar = partblueprint('assets/images/solarpanel.png',10,generator=10)
scaffolding = partblueprint('assets/images/scaffold.png',10)
rocket = rocketship(0,0,parts=[(0,0,0),(0,0,1),(0,0,3),(0,0,4),(1,0,5),(3,1,3),(3,-1,3),(2,2,3),(2,3,3),(2,-2,3),(2,-3,3)])
