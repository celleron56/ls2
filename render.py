
class camera():
    def __init__(self,x=0,y=0,zoomlevel=1):
        self.camerax = x
        self.cameray = y
        self.zoomlevel = zoomlevel
        self.smoothzoomlock = 0
        self.smoothzoomtarget = zoomlevel
        self.xtarget = x
        self.ytarget = y
        self.movelockx = 0
        self.movelocky = 0
        self.xdistance =  0
        self.ydistance = 0
        self.smoothzoomdistance = 0
        self.attachx = 0
        self.attachy = 0
        self.attachzm = 0
    def smoothchangezoomlevel(self,zoomlevel):
        if self.smoothzoomlock == 0:
            self.smoothzoomtarget =  zoomlevel
            self.smoothzoomlock = 1
            self.smoothzoomdistance = (self.smoothzoomtarget - self.zoomlevel)
        else:
            self.smoothzoomtarget = zoomlevel
            self.smoothzoomdistance = (self.smoothzoomtarget - self.zoomlevel)
    def smoothmove(self,x,y):
        self.movelockx = 1
        self.movelocky = 1
        self.xtarget = x + self.attachx
        self.xdistance = x - self.camerax
        self.ytarget = y + self.attachy
        self.ydistance = y - self.cameray
    def updzoom(self):
        if self.smoothzoomlock == 1:
            if ((self.smoothzoomtarget -1 ) < self.zoomlevel) and ((self.smoothzoomtarget +1 ) > self.zoomlevel):
                self.zoomlevel = self.smoothzoomtarget
                self.smoothzoomlock = 0
            else:
                self.zoomlevel = self.zoomlevel + self.smoothzoomdistance* 0.1

        if self.movelockx == 1:
            if ((self.xtarget -(abs(self.xdistance * 0.1) * 0.5)) < self.camerax) and ((self.xtarget +(abs(self.xdistance * 0.1) * 0.5) ) > self.camerax):
                self.camerax = self.xtarget
                self.movelockx = 0
            else:
                if self.xtarget < self.camerax :
                  self.camerax = self.camerax - abs(self.xdistance * 0.25)
                else:
                  self.camerax = self.camerax + abs(self.xdistance * 0.25)
        if self.movelocky == 1:
            if ((self.ytarget -(abs(self.ydistance * 0.1) * 0.5) ) < self.cameray) and ((self.ytarget + (abs(self.ydistance * 0.1) * 0.5) ) > self.cameray):
                self.cameray = self.ytarget
                self.movelocky = 0
            else:
                if self.ytarget < self.cameray :
                  self.cameray = self.cameray - abs(self.ydistance * 0.25)
                else:
                  self.cameray = self.cameray + abs(self.ydistance * 0.25)

    def getcx(self):
        return  self.attachx + self.camerax   
    
    def getcy(self):
        return  self.attachy + self.cameray 
    def getzm(self):
        return self.zoomlevel + self.attachzm
    
    def attach(self,x,y):
        zoomlevel = (0.01 * self.getzm())
        self.attachx = (250-x) * zoomlevel
        self.attachy = (250-y) * zoomlevel