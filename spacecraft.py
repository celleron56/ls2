partlist = []
from celestialobjects import *
import vidi
backgroundtile = pyglet.image.load('assets/images/background.png')
 ## 4 letter string 100x100 = 597.12 kb half a mb
## 4 letter string 128x128 = 969.8 kb almost a mb
## 6 letter string 128x128 = 1001.3 kb a megabyte
cursorimg = pyglet.image.load('assets/images/cursor.png')
class editorstage():
    def __init__(self):
        self.image = pyglet.image.load('assets/images/editor.png')
        self.positionx = -5
        self.positiony = 0
        self.prebatch = pyglet.graphics.Batch()
    def draw(self,viewport="none"):
        self.batch = self.prebatch
        self.images = []
        global debug
        for xt in range(-10,10):
            for yt in range(-10,10):
                if not viewport == "none":
                    x = ((self.positionx + ((xt * 0.5))) * 12 * (viewport.getzm() * 0.01))+ viewport.getcx()
                    y = ((self.positiony + ((yt * 0.5) ))*  12 *(viewport.getzm() * 0.01)) + viewport.getcy()
                    zoomlevel = ((viewport.getzm() * 0.01) + 0.00000001)
                    x = (x % 700) - 40
                    y = (y % 600) - 40
                else:
                    x = self.positionx + (xt * 21)
                    y = self.positiony +( yt * 21)
                    zoomlevel = 10
                if debug == 1:
                    print((x,y))
                self.images.append(pyglet.sprite.Sprite(self.image, x, y, batch=self.batch))
                t = self.images[len(self.images) -1]
                t.update(scale=(zoomlevel * 0.1) )
                self.images[len(self.images) -1] = t
        self.batch.draw()
        


class partblueprint():
    def __init__(self,texturepath,weigth=1,fuellevel=0,control=0,generator=0,fuelusage=0,thrustmode="none",code="",name="",mine=0,thrust=0,EU=0):
        global partlist
        self.part = [pyglet.image.load(texturepath),weigth,fuellevel,control,generator,fuelusage,thrustmode,code,name,thrust,EU]
        partlist.append(self.part) # electrisity usage is index 10 (accounting for pythons 0 list term) 
        
    
        
        
        
        
        
        
class rocketship():
    def mkflyready(self):
        for i in self.parts:
            if (len(self.partlist -1) > i[0]) and i[0] >= 0:
                partbp = self.partlist[i[0]] 
                self.fuel = self.fuel + partbp[2]
                thrustmode = partbp[6]
                if not thrustmode == "none":
                    if thrustmode == "main":
                        self.thrust = self.thrust + partbp[9]
                        self.aeithrust = self.aeithrust + partbp[10]
                    if thrustmode == "rcs":
                        self.rcs = self.rcs + partbp[9]
                        self.aeircs = self.aeircs + partbp[10]
                self.energygeneration = self.energygeneration + partbp[5]
                self.energygeneration = self.energygeneration + partbp[5]
                self.control = self.control + partbp[3]
                self.weight = self.weight + partbp[1]
    def rotate(self,degrees):
        if self.state[1] == 1 and self.fuel > self.aeircs:
            self.rotation = self.rotation + (degrees * 0.01) * (self.rcs +self.control)
            self.fuel = self.fuel  - self.aeircs
        else:
            self.rotation = self.rotation + (degrees * 0.01) * (self.control)
    def accelerate(self,amount):
        if self.state[0] > 0 and self.fuel > self.aethrust:
            multiplier = rotvec((amount * 0.01) * (self.thrust +self.control),(amount * 0.01) * (self.thrust +self.control),self.rotation)
            self.velocity[0] = self.velocity[0] + multiplier[0]
            self.velocity[1] = self.velocity[1] + multiplier[1]
            self.fuel = self.fuel  - self.aeithrust
        else:
            multiplier = rotvec((amount * 0.01) * self.control,(amount * 0.01) * self.control,self.rotation)
            self.velocity[0] = self.velocity[0] + multiplier[0]
            self.velocity[1] = self.velocity[1] + multiplier[1]
    def __init__(self,positionx,positiony,parts=[(0,0,0)]):
        global partlist
        self.state = [0,0,0,0,0] # first number is for using engine / engine power next num is for using rcs (no rcs power ) last 3 numbers for if various systems are on 
        self.velocity = [0,0,0] # first 2 vectors position , last  vector rotation
        self.fuel = 0
        self.planetkeepup = [0,0]
        self.thrust = 0
        self.rcs = 10
        self.gmult = 1
        self.weight = 0
        self.control =10 ## adds on to how quickly craft can be controlled later maybe could be used to implement probe cores ? 
        self.energy = 0 #battery
        self.energygeneration = 0 # solar pannels for example
        self.energyintake = 0 #passive energy intake or how much energy the system takes in (consumes)
        self.aeithrust = 0 # active energy intake main thrusters (measured in fuel intake )
        self.aeiscience = 0 # active energy intake science (electricity)
        self.aeircs = 0 # active energy intake rccs system (fuel)
        self.aeimachine = 0 # active energy machinery (energy)
        self.convert = 0 # converts this much ore into fuel 
        self.mine = 0 # mining abilities (pasively collects this much ore )
        self.partlist = partlist
        self.parts=parts
        self.positionx = positionx
        self.positiony = positiony
        self.prebatch = pyglet.graphics.Batch()
        self.batch = self.prebatch
        self.images = []
        self.rotation = 0
    def addpart(self,part,x,y):
        self.parts.append((part,x,y))
    def run(self,planets=[]):
        collided = 0
        self.rotation = self.rotation + self.velocity[2]
        if self.velocity[0] > 10:
            self.velocity[0] = 10
        if self.velocity[0] < -10:
            self.velocity[0] = -10
        if self.velocity[1] > 10:
            self.velocity[1] = 10
        if self.velocity[1] < -10:
            self.velocity[1] = -10
        for  i in range(1,4):
            self.collides(planets)
            self.positionx = self.positionx + (self.velocity[0] * 0.25)
            self.positiony = self.positionx + (self.velocity[1] * 0.25)
    def collides(self,planets=[]):
        for planet in planets:
            t = vidi.collides(self,planet)
            self.positionx = self.positionx + t[2] 
            self.positiony = self.positiony + t[3]
            self.velocity[0] +=  t[4]
            self.velocity[1] +=  t[5]
            if t[1] == True :
                self.velocity[0] =  math.floor(self.velocity[0]  * 9) * 0.1
                self.velocity[1] =  math.floor(self.velocity[1]  * 9) * 0.1
                self.velocity[2] =  math.floor(self.velocity[2]  * 9) * 0.1
            if t[0] == True :
                self.velocity[0] =  math.floor(self.velocity[0]  * 5) * 0.1
                self.velocity[1] =  math.floor(self.velocity[1]  * 5) * 0.1
                self.velocity[2] =  math.floor(self.velocity[2]  * 5) * 0.1
            
        
    def removepart(self,x,y):
        for partcounter in range(0,len(self.parts)) :
            part = self.parts[partcounter]
            if part[1] == x and part[2] == y :
                self.parts.pop(partcounter)
        
    def draw(self,viewport="none"):
        cursorx = 0
        cursory = 0
        cursordisp = False
        cursordisplayimg = "none"
        self.batch = self.prebatch
        global cursorimg , backgroundtile
        self.images = []
        global debug
        for partcounter in range(0,len(self.parts) ):
            part = self.parts[partcounter]
            global pi
            if not viewport == "none":
                relpartx = part[1] * 1.9
                relparty = part[2] * 1.9
                rotatedpart = rotvec(relpartx,relparty,self.rotation)
                y = (self.positiony +  rotatedpart[1]   * (viewport.getzm() * 0.01)) + viewport.getcy()
                x = (self.positionx + rotatedpart[0]    * (viewport.getzm() * 0.01)) + viewport.getcx()
                zoomlevel = ((viewport.getzm() * 0.01) + 0.00000001)
            else:
                rotatedpos = rotvec(part[1] * 19,part[2] * 19,self.rotation)
                x = self.positionx + rotatedpos[0]
                y = self.positiony + rotatedpos[1]
                zoomlevel = 10
            if debug == 1:
                print((x,y))
            if not part[0] == "cursor":
                if not part[0] == "BG" :
                    image = self.partlist[part[0] % len(self.partlist)]
                    image = image[0]
                else:
                    image = backgroundtile
                self.images.append(pyglet.sprite.Sprite(image, x, y, batch=self.batch))
                t = self.images[len(self.images) -1]
                t.update(rotation=self.rotation , scale=(zoomlevel * 0.1) )
                self.images[len(self.images) -1] = t
            else:
                cursorx = x
                cursory = y
                cursordisp = True
        
        self.batch.draw()
        if cursordisp == True:
            cursordisplayimg = pyglet.sprite.Sprite(cursorimg, cursorx, cursory)
            cursordisplayimg.update(rotation=self.rotation , scale=(zoomlevel * 0.1) )
            cursordisplayimg.draw()

    def gposition(self,time,height,speed):        
     #   if not self.attach == "no":
     #       self.attach.gposition(time)
      #      self.xposition = (math.sin(((time * speed) % 361 )* 0.01745329) * height) + self.attach.xposition
      #      self.yposition = (math.cos(((time * speed) % 361) * 0.01745329) * height) + self.attach.yposition
      #  else:
             self.xposition = self.xposition
             self.yposition = self.xposition
    def drawinorbit(self,time,attach,orbitalheight=10,viewport="none",speed=10):
        self.batch = self.prebatch
        self.images = []
        self.attach = attach
        global debug
        self.gposition(time,orbitalheight,speed)
        for partcounter in range(0,len(self.parts) ):
            part = self.parts[partcounter]
            global pi
            if not viewport == "none":
                x = (self.xposition +  part[1] * 1.9 * (viewport.getzm() * 0.01))+ viewport.getcx()
                y = (self.yposition +   part[2] * 1.9* (viewport.getzm() * 0.01)) + viewport.getcy()
                zoomlevel = ((viewport.getzm() * 0.01) + 0.00000001)
            else:
                x = self.positionx
                y = self.positiony
                zoomlevel = 10
            if debug == 1:
                print((x,y))
            image = self.partlist[part[0] % len(self.partlist)]
            image = image[0]
            self.images.append(pyglet.sprite.Sprite(image, x, y, batch=self.batch))
            t = self.images[len(self.images) -1]
            t.update(rotation=self.rotation , scale=(zoomlevel * 0.1) )
            self.images[len(self.images) -1] = t
        self.batch.draw()
        

class partview():
    def __init__(self,position_x , position_y):
        self.display = rocketship(position_x,position_y,[(1,0,0)])
        self.displaybackground = rocketship(position_x,position_y,[(1,0,0)])
        self.x = position_x
        self.y = position_y
        parts = []
        for i in range(0,5):
            parts.append(["BG",20,i])
        self.displaybackground.parts = parts
    def draw(self,selected=2):
        partlist = self.display.partlist
        parts = []
        parts.append(("cursor",20,2))
        for i in range(0,5):
            counter = (i- 2) + selected
            counter = (counter ) % (len(partlist )- 1)
            parts.append([copy.deepcopy(counter),20 ,copy.deepcopy(i)  ])
        self.displaybackground.draw()
        self.display.parts = parts
        self.display.draw()
        
        
        
















tank = partblueprint('assets/images/verticaltank.png',10,fuellevel=100)
nosecone = partblueprint('assets/images/nosecone.png',10,fuellevel=25)
solar = partblueprint('assets/images/solarpanel.png',10,generator=10)
scaffolding = partblueprint('assets/images/scaffold.png',10)
engine = partblueprint('assets/images/smallengine1.png',10,thrust=50,thrustmode="main",fuelusage=5)

crewpot = partblueprint('assets/images/crew_pot.png',10,control=10)

rcs = partblueprint('assets/images/rcs.png',10,thrust=20,fuelusage=1,thrustmode="rcs")


editorpartview = partview(-350,50)
rocket = rocketship(0,0,parts=[(0,0,0),(0,0,1),(0,0,3),(0,0,4),(1,0,5),(3,1,3),(3,-1,3),(2,2,3),(2,3,3),(2,-2,3),(2,-3,3)])
            
                