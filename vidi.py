## vehicle intercollision detection interface lib  
import math
import gravity as gt
#import pybox2d
from celestilib import rotvec
#precoldetectionworld = b2World()
# def collidesland(vehicle,planet):
#     global precoldetectionworld
#     cdworld = precoldetectionworld
#     spacecraftx =vehicle.positionx
#     spacecrafty=vehicle.positiony
#     spacecraftgrx = vehicle.grx(vehicle.rotation)
#     spacecraftgry = vehicle.gry(vehicle.rotation)
#     planetx = planet.xposition
#     planety = planet.yposition
#     mindist = planet.size + 20
#     planetcollider = cdworld.CreateStaticBody(
#         position=(0,-10),
#         shapes= b2CircleShape(pos=(planetx, planety), radius=mindist),
#     )
#     collides = False
#     for part in vehicle.parts:
#         relpartx = part[1] * 1.9
#         relparty = part[2] * 1.9
#         rotatedpart = rotvec(relpartx,relparty,vehicle.rotation)
#         part_x = spacecraftx + rotatedpart[0]
#         part_y = spacecrafty + rotatedpart[1]
#         partdisttoplanet = math.dist((part_x,part_y),(planetx,planety))
#         if partdisttoplanet < (mindist):
#            collides = True
#     del(cdworld)
#     if collides == True:
#         return 1
#     else:
#         return 0
    
def collides(vehicle,planet):
    spacecraftx =vehicle.positionx
    spacecrafty=vehicle.positiony
    planetx = planet.xposition
    planety = planet.yposition
    mindistathmosphere = (planet.size * 19) + 20 + (planet.atmospheredist * 10)
    collides = False
    inatmosphere = False
    mindistground = (planet.size * 19) 
    movedistx = 0
    movedisty = 0
    gravity_x = 0
    gravity_y = 0
    for part in vehicle.parts:
        relpartx = part[1] * 1.9
        relparty = part[2] * 1.9
        rotatedpart = rotvec(relpartx,relparty,vehicle.rotation)
        part_x = spacecraftx + rotatedpart[0]
        part_y = spacecrafty + rotatedpart[1]
        gravity_x = 0
        gravity_y = 0
        partdisttoplanet = math.dist((part_x,part_y),(planetx,planety))
       # print(str(partdisttoplanet) + "min dist : " + str(mindistground) + " " + str(planet.name))
        if partdisttoplanet < (mindistathmosphere):
           inatmosphere = True
           
        if partdisttoplanet < (mindistground):
           collides = True
           delta_x = ( part_x - planetx )
           delta_y = ( part_y - planety )
           degrees_temp = math.atan2(delta_x, delta_y)/math.pi*180
           reversedmindist =   (mindistground  - partdisttoplanet)
           suggestedcoords = rotvec(0,reversedmindist,degrees_temp)
           suggestedx =  suggestedcoords[0]
           suggestedy =  suggestedcoords[1]
           if abs(suggestedx ) > abs(movedistx):
               movedistx =  suggestedx
           if abs(suggestedy) > abs(movedisty):
               movedisty =  suggestedy
           
           
    if ( collides == False):
        planet.gposition(planet.time)
        gravityobjspacecraft = gt.gravityobject((spacecraftx , spacecrafty),(vehicle.velocity[0],vehicle.velocity[1]),vehicle.weight)
        gravityobjplanet = gt.gravityobject((planety , planetx),(0,0),planet.size * planet.gmult)
        gravityobjspacecraft = gt.gravity(gravityobjspacecraft,gravityobjplanet)
        gravity_x =  gravityobjspacecraft.xvel
        gravity_y =  gravityobjspacecraft.yvel 
            
    return [collides,inatmosphere,movedistx,movedisty,gravity_x,gravity_y]


